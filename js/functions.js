function readDeviceOrientation() {
	// window.innerHeight is not supported by IE
	var winH = window.innerHeight ? window.innerHeight : jQuery(window).height()
	var winW = window.innerWidth ? window.innerWidth : jQuery(window).width()
	//force height for iframe usage
	if (!winH || winH == 0) {
		winH = '100%'
	}
	// set the height of the document
	jQuery('html').css('height', winH)
	// scroll to top
	window.scrollTo(0, 0)
}

function accessWebVr(curScene) {
	$('#header, #nav, #copyright, #help').prependTo('#tour .row')
	unloadPlayer()

	setTimeout(function () {
		loadPlayer(true, curScene)
		$('#header, #nav').removeClass('show')
	}, 100)
}

function accessStdVr(curScene) {
	$('#header, #nav, #copyright, #help').prependTo('#krpanoSWFObject')
	unloadPlayer()

	setTimeout(function () {
		loadPlayer(false, curScene)
		$('#header, #nav').addClass('show')
	}, 100)
}

function loadPlayer(isWebVr, curScene) {
	if (isWebVr) {
		embedpano({
			id: "krpanoSWFObject"
			, xml: "Belas_Artes_finaldata/Belas_Artes_final.xml"
			, target: "panoDIV"
			, passQueryParameters: true
			, bgcolor: "#010509"
			, html5: "only+webgl"
			, consolelog: "true"
			, wmode: "transparent"
			, focus: false
			, vars: { skipintro: true, norotation: true, startscene: curScene }
			, mwheel: true
			, capturetouch: false
			, touchdevicemousesupport: true
			, mobilescale: 0.5
			, consolelog: true
		})

	} else {

		var isBot = /bot|googlebot|crawler|spider|robot|crawling/i.test(navigator.userAgent)
		embedpano({
			id: "krpanoSWFObject"

			// , swf: "Belas_Artes_finaldata/Belas_Artes_final.swf"
			, xml: "Belas_Artes_finaldata/Belas_Artes_final.xml"

			, target: "panoDIV"
			, passQueryParameters: true
			, bgcolor: "#010509"
			, focus: false
			, html5: "always"
			, consolelog: "true"
			, wmode: "transparent"
			, vars: { startscene: curScene }
			, mwheel: true
			, capturetouch: false
			, touchdevicemousesupport: true
			, mobilescale: 0.5
			, consolelog: true

		})
	}

	//apply focus on the visit if not embedded into an iframe
	// if (top.location === self.location) {
	// 	kpanotour.Focus.applyFocus()
	// }


	$('#tour header, #tour nav, #tour #copyright, #help').prependTo('#krpanoSWFObject')

}

function unloadPlayer() {
	if (jQuery('#krpanoSWFObject')) {
		$('#tour header, #tour nav, #tour #copyright, #help').prependTo('#tour .row')
		removepano('krpanoSWFObject')
	}
}

function isVRModeRequested() {
	var querystr = window.location.search.substring(1)
	var params = querystr.split('&')
	for (var i = 0; i < params.length; i++) {
		if (params[i].toLowerCase() == "vr") {
			return true
		}
	}
	return false
}

function init() {
	var isFirefox = /Android.+Firefox\//.test(navigator.userAgent);

	if (isFirefox) {
		// ...
	}

}

function enableStartButtons() {
	var isFirefox = /Android.+Firefox\//.test(navigator.userAgent);

	$('.text-loading').html('Pronto!').addClass('hidden').delay(1150).queue(function () {
		$('#tour-tab').removeClass('disabled')
		$('#tour-tab').removeClass('invisible')
	})
}

function moveTop(e) {
	switch (e) {
		case '#krpanoSWFObject':
			$('#tour header, #tour nav, #tour #copyright, #help').prependTo('#krpanoSWFObject')
			break;

		default:
			$('#tour header, #tour nav, #tour #copyright, #help').prependTo('#tour .row')
			break;
	}
}

function currentPanoTitle(title) {
	$('#breadcrumb').find('.active').html(title)
}

function loadMenu() {
	var scenes = this.responseXML.documentElement.getElementsByTagName('scene')
	var krpano = document.getElementById('krpanoSWFObject')
	var divider = '<div class="dropdown-divider"></div>'
	var i, tag, scene

	for (i = 0; i < scenes.length; i++) {
		tag = scenes[i].getAttribute('tag')

		if (tag === 'works') {
			scene = divider + '<a class="dropdown-item" href="#" id="' + scenes[i].getAttribute('name') + '">' + scenes[i].getAttribute('titleid') + '</a>'
			$('.scenes').find('.dropdown-menu').append(scene)
		}
	}

	$('.scenes').find('.dropdown-item').click(function (e) {
		e.preventDefault()
		loadScene($(this).attr('id'))
	})
}

function hotspotsMenu() {
	var item, tag
	var divider = '<div class="dropdown-divider"></div>'
	var krpano = document.getElementById('krpanoSWFObject')
	var hotspot_count = krpano.get('hotspot.count')
	var dropdown_menu = $('.works').find('.dropdown-menu')

	dropdown_menu.empty()

	for (i = 0; i < hotspot_count; i++) {
		tag = krpano.get('hotspot[ ' + i + '].tag')
		if (tag === 'work') {
			item = divider + '<a class="dropdown-item dropdown-item-work" href="#" id="' + krpano.get('hotspot[ ' + i + '].name') + '" data-ath="' + krpano.get('hotspot[ ' + i + '].ath') + '" data-atv="' + krpano.get('hotspot[ ' + i + '].atv') + '">' + krpano.get('hotspot[ ' + i + '].hotspot_author') + '</a>'
			dropdown_menu.append(item)
		}
	}

	$('.dropdown-item-work').click(function () {
		var ath = $(this).data('ath')
		var atv = $(this).data('atv')
		var krpano = document.getElementById('krpanoSWFObject')
		krpano.call('moveto(' + ath + ', ' + atv + ', smooth());')
	})

}

function toggleSearch(tag) {
	console.log(tag)
	tag === 'works' ? $('.works').removeClass('d-none') : $('.works').addClass('d-none')
}

function loadScene(id) {
	var krpano = document.getElementById('krpanoSWFObject')
	krpano.call('mainloadscene(' + id + ')')
}

function hideBars() {
	var header = $('#header')
	var nav = $('#nav')

	header.fadeOut('fast')
	nav.fadeOut('fast')
}

function showBars() {
	var header = $('#header')
	var nav = $('#nav')

	header.fadeIn('fast')
	nav.fadeIn('fast')
}

function toggleHelp() {
	var help = $('#help')
	help.hasClass('show') ? help.removeClass('show') : help.addClass('show')
}

function getHotspots(scene) {
	var json = "js/database/" + scene + ".json"
	$.getJSON(json, function (data) {
		// var result = $.each(data, function(key, val){ if(key === scene) console.log(val) })

		var krpano = document.getElementById('krpanoSWFObject')
		var cases = data.cases

		$.each(cases, function (index, element) {

			krpano.call("createSpotwork(spotwork_" + index + ", " + element.link + ", " + element.video + ", " + data.directory + ", " + element.gallery + ")")

			krpano.set("hotspot[spotwork_" + index + "].hotspot_title", element.title)
			krpano.set("hotspot[spotwork_" + index + "].hotspot_author", element.author)

		})

	});
}

function stopVideo(video) {
	var playerID = '#' + video + '_html5videoplayer';
	var player = document.querySelector(playerID);
	var krpano = document.querySelector('#krpanoSWFObject');
	player.pause();
	krpano.call('set(hotspot[videoplayer_toggle].url, images/icons/play.png)');
}

function toggleVideo(video) {
	var playerID = '#' + video + '_html5videoplayer';
	var player = document.querySelector(playerID);
	var krpano = document.querySelector('#krpanoSWFObject');

	if (player.paused == true) {
		player.play();
		krpano.call('set(hotspot[videoplayer_toggle].url, images/icons/pause.png)');
	} else {
		player.pause();
		krpano.call('set(hotspot[videoplayer_toggle].url, images/icons/play.png)');
	}
}

function vimeoPlayer(id) {
	var iframe = $('#vimeoModal').find('iframe');
	var options = {
				title: false
		};
	var player = new Vimeo.Player(iframe, options);
	// var krpano = document.querySelector('#krpanoSWFObject');

	// player.on('play', function () {
	// 	console.log('played the video! ' + videoID);
	// 	krpano.call('moveto(0.000001, 0.000000); delayedcall(0.3, moveto(0.000000, 0.000000););');
	// });

	player.getVideoTitle().then(function (title) {
		// console.log('title:', title);
	});
}

function vimeoPause(id) {
	var videoID = '#' + id + '_iframe';
	var iframe = document.querySelector(videoID);
	var player = new Vimeo.Player(iframe);

	player.pause().then(function () {
		// the video was paused
	}).catch(function (error) {
		switch (error.name) {
			case 'PasswordError':
				// the video is password-protected and the viewer needs to enter the
				// password first
				break;

			case 'PrivacyError':
				// the video is private
				break;

			default:
				// some other error occurred
				break;
		}
	});
}

function modalOpen(id) {
	var iframe = '<iframe src="https://player.vimeo.com/video/'+id+'" width="640" style="min-height:360px; max-height:480px; height:100%; background-color:#000000;" frameborder="0" allowfullscreen >';

	$('#vimeoModal').on('show.bs.modal', function (event) {
		var modal = $(this)
		modal.find('.modal-body').prepend(iframe)
	})

	$('#vimeoModal').on('shown.bs.modal', function (event) {
		vimeoPlayer()
	})

	$('#vimeoModal').on('hidden.bs.modal', function (event) {
		var modal = $(this)
		var krpano = document.querySelector('#krpanoSWFObject')

		iframe = ''

		modal.find('.modal-body').empty()
		krpano.call('destroyVideoplayer()')
	})

	$('#vimeoModal').modal('show')
}