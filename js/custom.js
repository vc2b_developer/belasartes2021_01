// Custom actions 

(function($) {
  "use strict"; // Start of use strict
  
  init();

  if (/(iphone|ipod|ipad|android|iemobile|webos|fennec|blackberry|kindle|series60|playbook|opera\smini|opera\smobi|opera\stablet|symbianos|palmsource|palmos|blazer|windows\sce|windows\sphone|wp7|bolt|doris|dorothy|gobrowser|iris|maemo|minimo|netfront|semc-browser|skyfire|teashark|teleca|uzardweb|avantgo|docomo|kddi|ddipocket|polaris|eudoraweb|opwv|plink|plucker|pie|xiino|benq|playbook|bb|cricket|dell|bb10|nintendo|up.browser|playstation|tear|mib|obigo|midp|mobile|tablet)/.test(navigator.userAgent.toLowerCase())) {
    if(/iphone/.test(navigator.userAgent.toLowerCase()) && window.self === window.top){
      jQuery('body').css('height', '100.18%')
    }
    // add event listener on resize event (for orientation change)
    if (window.addEventListener) {
      window.addEventListener("load", readDeviceOrientation)
      window.addEventListener("resize", readDeviceOrientation)
      window.addEventListener("orientationchange", readDeviceOrientation)
    }
    //initial execution
    setTimeout(function(){readDeviceOrientation()},10)
  }

  $('#tour-tab').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
  })

  $('#webvr-tab').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
    var krpano = document.getElementById('krpanoSWFObject')
    accessWebVr(krpano.get('xml.scene'))
  })

  $('a[href="#tour"]').on('shown.bs.tab', function (e) {
    e.target
    e.relatedTarget

    var krpano = document.getElementById('krpanoSWFObject')

    // switch top
    if(e.target.id !== 'webvr-tab'){
      $('#header, #nav').addClass('show')
      setTimeout(function(){
        krpano.call('toggle(fullscreen)')
        // toggleHelp()
      }, 250);
      setTimeout(function(){
        krpano.call('custom_init()')
      }, 300);
    }
  })

  $('#btn-fullscreen').click(function(){
    var krpano = document.getElementById('krpanoSWFObject')
    krpano.call('toggle(fullscreen)')
  })

  $('#btn-webvr').click(function(){
    var krpano = document.getElementById('krpanoSWFObject')
    accessWebVr(krpano.get('xml.scene'))
  })

  $('#btn-help, #help .close, #help a').click(function(){
    var krpano = document.getElementById('krpanoSWFObject')
    krpano.set('storedscene', krpano.get('xml.scene'))
    krpano.call('mainloadscene(panohelp)')
    // toggleHelp()
  })

})(jQuery)